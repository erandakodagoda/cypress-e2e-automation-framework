# Cypress QA E2E Automation

This is the Markdown file in **Cypress E2E Automation**. If you want to learn about Cypress Framework, you can read this file.

## Architecture

Cypress E2E Framework has been developed using Cypress which is trending automation tool which mostly used for React Based applications along with Cucumber JS for the BDD implementation. 

```mermaid
graph TD
    A[Web] -->|HTTP Proxy| B(Proxy Server)
    B --> C{Response}
    C -->|Web Socket| D[Browser]
    C -->|HTTP Response| D[Browser]
```
## Create files and folders

In the Project source code under the directory of cypress there is another directory called integration all the source codes will be there along with gherkin feature files. The files are named in features and each feature has a folder created with the same name to hold the step definition file which includes the execution steps of each scenarios.

## Rename a file

You can rename the current file by clicking the file name in the navigation bar or by clicking the **Rename** from your editor. **PS: When your changing the file name of the feature keep remember to change the folder name to the feature file name which holds the step definition**

## Delete a file

You can delete the current file by clicking the **Remove** button in the file explorer. The file will be moved into the **Trash** and remember to delete the step folder which contained the step definition along with it.

### Usage

Sample **.feature** file
```gherkin
Feature: As an Admin I should be able to login to the application by providing email and password incorrect email and passwords should be validated.

Scenario Outline:As an Admin I should be able to login to application using Correct email and password.
Given I see the Login Page
When I type "<email>" on email field
And I type "<password>" on password field
And I click on Sign In button 
Then I see admin panel new post element
Examples:
| email | password|
| test@test.com |1234@com|
```

Sample **Step Definition** File
```javascript
When('I see the Login Page', ()=>{
    cy.visit('url')
})
When('I type {string} on email field', (constant) => {
    const loginPageObjects = new LoginPageObjects()
    loginPageObjects.getEmailAddress().type(constant)
})
When('I type {string} on password field', (constant)=>{
    const loginPageObjects = new LoginPageObjects()
    loginPageObjects.getPasswordField().type(constant)
})
Then('I click on Sign In button',()=>{
    const loginPageObjects = new LoginPageObjects()
    loginPageObjects.getLoginButton().click()
});
Then('I see admin panel new post element',()=>{
    const adminPanelPageObjects = new AdminPanelPageObjects()
    adminPanelPageObjects.getYourFeed().should('be.visible')
})
```
Sample **Page Object** class
```javascript
class loginPageObject{
    getLoginLabelText(){
        return cy.get('locator');
    }
    getEmailAddress(){
        return cy.get('locator');
    }
    getPasswordField(){
        return cy.get('locator');
    }
    getLoginButton(){
        return cy.get('locator').contains('text');
    }
}
export default loginPageObject
```
### Installation

Cypress E2E Framework requires [Node.js](https://nodejs.org/) v12.8+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd test-automation
$ npm install 
$ npx cypress open (Open the Cypress and execute)
$ npm run test (Run in Headless Mode)
```

### Plugins

Cypress E2E Framework is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin | Documentation |
| ------ | ------ |
| Cypress | [cypress/documentation][PlDb] |
| Cucumber | [cucumberjs/documentation][PlGh] |
| Mocha | [mochajs/documentation][PlDh]|

## License
----
All Rights Received by Eranda Kodagoda.

   [PlDh]: <https://mochajs.org/#getting-started>
   [PlGh]: <https://cucumber.io/docs/gherkin/reference/>
   [PlDb]: <https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell>