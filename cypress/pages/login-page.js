export default class LoginPage{
    getName(constant){
        cy.get('div:nth-child(1) > input').type(constant);
        return this
    }
    getEmailAddress(constant){
        cy.get('input[name="email"]').type(constant);
        return this
    }
    getPasswordField(constant){
        cy.get('input[id="exampleInputPassword1"]').type(constant);
        return this
    }
}
