Feature: As an Admin I should be able to login to the application by providing email and password incorrect email and passwords should be validated.

Scenario Outline:As an Admin I should be able to login to application using Correct email and password.
Given I see the Login Page
When I type "<name>" on name field
And I type "<email>" on email field
Then I type "<password>" on password field
And I wait for 5 seconds

Examples:
|name | email | password|
|Eranda| test@test.com |1234@com|
