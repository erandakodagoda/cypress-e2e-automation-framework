
import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps'
import LoginPage from '../../pages/login-page'
const loginPage = new LoginPage();

When("I see the Login Page", ()=>{
    cy.visit("https://rahulshettyacademy.com/angularpractice/")
});
When("I type {string} on name field", (name)=>{
    
    loginPage.getName(name)
});
When("I type {string} on email field", (email)=>{
    loginPage.getEmailAddress(email)
});
When("I type {string} on password field", (password)=>{
    loginPage.getPasswordField(password)
});
When("I wait for {int} seconds", (number)=>{
    cy.wait(number)
});